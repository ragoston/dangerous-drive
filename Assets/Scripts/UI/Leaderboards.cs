﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.Linq;

public class Leaderboards : Navigation
{
    [SerializeField]
    private List<Text> highscores;
    private PlayerData mData;
    private Button back;
    
    public override void setup()
    {
        mData = GameObject.Find("GameLogic").GetComponent<PlayerData>();
        back = transform.FindChildRecursive("back").GetComponent<Button>();
        back.onClick.AddListener(() =>
        {
            navigations.Where(x => x.serial == "home").FirstOrDefault().gameObject.SetActive(true);
            gameObject.SetActive(false);
        });
    }
    public override void onEnabled()
    {
        long[] sorted = mData.MSave.leaderboards;
        Array.Sort(sorted);
        for (int i = sorted.Length-1; i >= 0; i--)
        {
            if(sorted[i] > 0)
            {
                highscores[sorted.Length - i - 1].text = sorted[i].ToString();
            }           
        }
    }
}
