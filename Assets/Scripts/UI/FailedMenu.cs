﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using System;

public class FailedMenu : Navigation
{
    private IGameRestarter pRest;

    private Button restart;
    private Text score;

    //navigation buttons
    private Button skins;
    private Button sound;
    private Button carSettings;
    private Button leaderBoards;
    private AudioListener mListener;

    public Sprite muteSprite;
    public Sprite soundSprite;
    private Image soundImage;

    private PlayerData mData;

    private void Awake()
    {
        pRest = UnityEngine.Object.FindObjectsOfType(typeof(MonoBehaviour)).Where(x => x is IGameRestarter)
    .FirstOrDefault() as IGameRestarter;
    }

    internal void setup()
    {
        soundImage = transform.FindChildRecursive("SoundImage").GetComponent<Image>();
        mListener = Camera.main.GetComponent<AudioListener>();
        restart = transform.FindChildRecursive("Restart").GetComponent<Button>();
        restart.onClick.AddListener(() => {
            pRest.OnRestart();
        });
        mData = GameObject.Find("GameLogic").GetComponent<PlayerData>();

        if (!mData.MSave.soundEnabled)
        {
            soundImage.sprite = muteSprite;
        }
        else soundImage.sprite = soundSprite;

        skins = transform.FindChildRecursive("Skins").GetComponent<Button>();
        sound = transform.FindChildRecursive("Sound").GetComponent<Button>();
        carSettings = transform.FindChildRecursive("CarSettings").GetComponent<Button>();
        leaderBoards = transform.FindChildRecursive("Leaders").GetComponent<Button>();
        score = transform.FindChildRecursive("CurrentScore").GetComponent<Text>();

        skins.onClick.AddListener(() => {
            navigations.Where(x => x.serial.Equals("skins")).FirstOrDefault().gameObject.SetActive(true);
            gameObject.SetActive(false);
        });
        sound.onClick.AddListener(() => {
            if (mData.MSave.soundEnabled)
            {
                mData.MSave.soundEnabled = false;
                AudioListener.volume = 0f;
                soundImage.sprite = muteSprite;
            }
            else
            {
                mData.MSave.soundEnabled = true;
                AudioListener.volume = 1f;
                soundImage.sprite = soundSprite;
            }
            mData.SaveData();
        });
        carSettings.onClick.AddListener(() => {
            navigations.Where(x => x.serial.Equals("carSettings")).FirstOrDefault().gameObject.SetActive(true);
            gameObject.SetActive(false);
        });
        leaderBoards.onClick.AddListener(() => {
            var _lb = 
            navigations.Where(x => x.serial.Equals("leaderboards")).FirstOrDefault();
            _lb.gameObject.SetActive(true);
            _lb.onEnabled();
            gameObject.SetActive(false);
        });
    }

    public void SetCurrentScore(long score)
    {
        this.score.text = score.ToString();
    }
}
