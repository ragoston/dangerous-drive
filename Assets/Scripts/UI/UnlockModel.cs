﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class UnlockModel : MonoBehaviour
{
    private string mName;
    public string Name { get { return mName; } }
    public enum ModelType { car, environment, engine}
    //set by inspector
    public ModelType mType;
    public Button mButton;
    public Text price;
    public Image mImage;
    public Image coin;
    public Sprite mUnlockedImage;

    //and actual data for that shit

    public int mPrice;
    public bool isUnlocked;

    private static PlayerData mData;

    public void Setup(PlayerData data)
    {
        if(mData == null)
            mData = data;
        mButton = GetComponent<Button>();
        mImage = transform.Find("viewer").GetComponent<Image>();
        price = transform.FindChildRecursive("price").GetComponent<Text>();
        coin = transform.FindChildRecursive("Image").GetComponent<Image>();
        mName = gameObject.name;
        switch (mType)
        {
            case ModelType.car:
                SetUnlockMe(data.MSave.cars.Where(x => x.name == Name).FirstOrDefault().isUnlocked);
                break;
            case ModelType.environment:
                SetUnlockMe(data.MSave.environments.Where(x => x.name == Name).FirstOrDefault().isUnlocked);
                break;
            case ModelType.engine:
                SetUnlockMe(data.MSave.engines.Where(x => x.name == Name).FirstOrDefault().isUnlocked);
                break;
            default:
                break;
        }
    }

    public void UpdateAppearance()
    {
        //using the class's own isUnlocked.
        if (isUnlocked)
        {
            mImage.sprite = mUnlockedImage;
            mImage.color = Color.white;
            Color transp = new Color(0, 0, 0, 0);
            coin.color = transp;
            price.color = transp;
        }
        else
        {
            //mImage is the ??? image by default.
            price.text = mPrice.ToString();

        }
    }

    public void SetUnlockMe(bool isUnlocked)
    {
        this.isUnlocked = isUnlocked;
        UpdateAppearance();
    }
}
