﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    private Image fuelFill;
    private Text distance;

    private void Start()
    {
        fuelFill = transform.FindChildRecursive("FuelFill").GetComponent<Image>();
        distance = transform.FindChildRecursive("Distance").GetComponent<Text>();
    }

    public void SetFillImage(float amount)
    {
        fuelFill.fillAmount = amount;
    }

    public void SetDistanceProgress(long score)
    {
        distance.text = score.ToString();
    }
}
