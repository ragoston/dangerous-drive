﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class Engines : Navigation
{
    private Button back;
    private Button unlock;
    private Image viewer;
    private PlayerData mData;
    private Image coinImage;
    private Text tPrice;
    public Sprite[] engines;
    public int[] prices;
    private int firstLocked;
    public override void setup()
    {       
        mData = GameObject.Find("GameLogic").GetComponent<PlayerData>();
        firstLocked = FirstLockedIndex();
        viewer = transform.FindChildRecursive("viewer").GetComponent<Image>();
        tPrice = transform.FindChildRecursive("price").GetComponent<Text>();
        coinImage = transform.FindChildRecursive("Image").GetComponent<Image>();
        back = transform.FindChildRecursive("back").GetComponent<Button>();
        back.onClick.AddListener(() =>
        {
            navigations.Where(x => x.serial == "home").FirstOrDefault().gameObject.SetActive(true);
            gameObject.SetActive(false);
        });

        unlock = transform.FindChildRecursive("unlockEngine").GetComponent<Button>();
        unlock.onClick.AddListener(() =>
        {
            if(firstLocked != -1)
            {
                if(mData.MSave.coins >= prices[firstLocked])
                {
                    mData.MSave.coins -= prices[firstLocked];
                    mData.MSave.engines[firstLocked].isUnlocked = true;
                    firstLocked = FirstLockedIndex();
                    mData.SaveData();
                    //now update UI
                    UpdateUI();
                }
            }
        });
        UpdateUI();
    }

    private void UpdateUI()
    {
        if (firstLocked == -1)
        {
            viewer.sprite = engines[engines.Length - 1];
            Color transp = new Color(0, 0, 0, 0);
            coinImage.color = transp;
            tPrice.color = transp;
        }           
        else
        {
            viewer.sprite = engines[firstLocked];
            tPrice.text = prices[firstLocked].ToString();
        }
    }

    private int FirstLockedIndex()
    {
        for (int i = 0; i < mData.MSave.engines.Length; i++)
        {
            if (!mData.MSave.engines[i].isUnlocked)
                return i;
        }
        return -1;
    }
}
