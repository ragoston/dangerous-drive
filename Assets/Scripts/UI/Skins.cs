﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class Skins : Navigation
{
    private Button back;
    private Button _switch;
    private List<UnlockModel> cars = new List<UnlockModel>();
    private List<UnlockModel> environments = new List<UnlockModel>();
    protected GameObject carsPanel;
    protected GameObject envirsPanel;
    private PlayerData mData;

    private ISwitchableLogo switchLogo;

    public override void setup()
    {
        mData = GameObject.Find("GameLogic").GetComponent<PlayerData>();
        carsPanel = transform.FindChildRecursive("cars").gameObject;
        envirsPanel = transform.FindChildRecursive("environments").gameObject;

        switchLogo = new Switch_Car(this);
        GameObject.FindGameObjectsWithTag("car_model").ToList().ForEach(
            x =>
            {
                var _cars = x.GetComponent<UnlockModel>();
                _cars.Setup(mData);
                cars.Add(_cars);
            });

        GameObject.FindGameObjectsWithTag("envir_model").ToList().ForEach(
            x =>
            {
                var _env = x.GetComponent<UnlockModel>();
                _env.Setup(mData);
                environments.Add(_env);               
            });
        //TODO engines panel.
        back = transform.FindChildRecursive("back").GetComponent<Button>();
        _switch = transform.FindChildRecursive("switch").GetComponent<Button>();

        back.onClick.AddListener(() =>
        {
            navigations.Where(x => x.serial.Equals("home")).FirstOrDefault().gameObject.SetActive(true);
            gameObject.SetActive(false);
        });

        _switch.onClick.AddListener(() =>
        {
            switchLogo.mPanel.SetActive(false);
            switchLogo = switchLogo.Other;
            switchLogo.mPanel.SetActive(true);
            _switch.GetComponentInChildren<Text>().text = switchLogo.Logo;
        });

        cars.ForEach(x => x.mButton.onClick.AddListener(() =>
        {
            Debug.Log("clicked on meh bitch");
            if (x.isUnlocked)
            {
                mData.MSave.selected_car_name = x.Name;
                Debug.Log("selected dat car you had.");
                mData.SaveData();
            }
            else
            {
                if (mData.MSave.coins >= x.mPrice)
                {
                    //clicked on it, now unlock it. 
                    mData.MSave.coins -= x.mPrice;
                    //TODO SAVE TO DRIVE
                    var mCar =
                    mData.MSave.cars.Where(y => y.name == x.Name)
                    .FirstOrDefault();
                    mCar.isUnlocked = true;
                    mData.MSave.selected_car_name = mCar.name;
                    mData.SaveData();
                    x.SetUnlockMe(true);
                }
            }
        }));

        environments.ForEach(x => x.mButton.onClick.AddListener(() =>
        {
            Debug.Log("environment clicked fam");
            if (x.isUnlocked)
            {
                mData.MSave.selected_env_name = x.Name;
                mData.SaveData();
            }
            else
            {
                if (mData.MSave.coins >= x.mPrice)
                {
                    //clicked on it, now unlock it. 
                    mData.MSave.coins -= x.mPrice;
                    //TODO SAVE TO DRIVE
                    var mEnv =
                    mData.MSave.environments.Where(y => y.name == x.Name)
                    .FirstOrDefault();
                    mEnv.isUnlocked = true;
                    mData.MSave.selected_env_name = mEnv.name;
                    mData.SaveData();
                    x.SetUnlockMe(true);
                }
            }
        }));
        envirsPanel.SetActive(false);
    }


    private class Switch_Car : ISwitchableLogo
    {
        private Skins mParent;
        public Switch_Car(Skins parent)
        {
            mParent = parent;
        }
        public GameObject mPanel { get { return mParent.carsPanel; } }
        public string Logo { get { return "CARS"; } }
        public ISwitchableLogo Other
        {
            get { return new Switch_Envir(mParent); }
        }
    }

    private class Switch_Envir : ISwitchableLogo
    {
        private Skins mParent;
        public Switch_Envir(Skins parent)
        {
            mParent = parent;
        }
        public string Logo { get { return "ENVIRONMENTS"; } }
        public ISwitchableLogo Other { get { return new Switch_Car(mParent); } }
        public GameObject mPanel { get { return mParent.envirsPanel; } }
    }

    internal interface ISwitchableLogo
    {
        string Logo { get; }
        ISwitchableLogo Other { get; }
        GameObject mPanel { get; }
    }
}