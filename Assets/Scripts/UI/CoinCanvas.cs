﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinCanvas : MonoBehaviour
{
    public float addonTime = 3f;

    private Text coins;
    private PlayerData pd;
    private int oCoin;
    private int nCoin;
    private Text addon;
    private void Awake()
    {
        coins = transform.FindChildRecursive("Text").GetComponent<Text>();
        addon = transform.FindChildRecursive("addon").GetComponent<Text>();
        pd = GameObject.Find("GameLogic").GetComponent<PlayerData>();
        coins.text = pd.MSave.coins.ToString();
        addon.text = "";
    }

    public void ShowAddon(int num)
    {
        StartCoroutine(_showAddon(num));
    }

    private IEnumerator _showAddon(int num)
    {
        if(num > 0)
        {
            addon.text = "+" + num.ToString();
        }
        yield return new WaitForSecondsRealtime(addonTime);
        addon.text = "";
    }

    private void LateUpdate()
    {
        nCoin = pd.MSave.coins;
        if(nCoin != oCoin)
        {
            oCoin = nCoin;
            coins.text = oCoin.ToString();
        }
    }
}
