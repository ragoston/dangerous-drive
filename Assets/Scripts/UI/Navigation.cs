﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Navigation : MonoBehaviour
{
    public string serial;

    protected static List<Navigation> navigations = new List<Navigation>();
    public static UnityAction<Scene> UnhookReferences;
    private void Awake()
    {
        UnhookReferences = x => { navigations = new List<Navigation>(); };
        SceneManager.sceneUnloaded += UnhookReferences;
        if (navigations.Count <= 0)
        {
            GameObject.FindGameObjectsWithTag("NavMenu").ToList().ForEach(y => { navigations.Add(y.GetComponent<Navigation>()); });
            navigations.Add(GameObject.FindGameObjectWithTag("FailMenu").GetComponent<Navigation>());
        }
    }
    public virtual void setup() { }
    public virtual void onEnabled() { }
}
