﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Buffer<T> : List<T>
{
    public Buffer()
    {

    }

    public Buffer(IList<T> input)
    {
        ReadArray(input);
    }

    public new T this[int i]
    {
        get
        {
            if (base.Count == 0) return default(T);
            while (i < 0)
            {
                i += base.Count;
            }
            return base[i % base.Count];
        }
        set
        {
            base[i] = value;
        }
    }

    public void Slide()
    {
        //move the last to be the first
        //and of course move everything 1 behind.
        T temp = base[0];
        for (int i = 0; i < base.Count-1; i++)
        {
            base[i] = base[i + 1];
        }
        base[base.Count - 1] = temp;
    }

    public void ReadArray(IList<T> input)
    {
        for (int i = 0; i < input.Count; i++)
        {
            base.Add(input[i]);
        }
    }
}
