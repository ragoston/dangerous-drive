﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour
{
    private AudioSource music;
    public AudioClip intro;
    public AudioClip loop;
    private void Awake()
    {
        music = transform.Find("musicsource").GetComponent<AudioSource>();
    }

    private void Start()
    {
        music.clip = intro;
        StartCoroutine(PlayLoop());

    }

    private IEnumerator PlayLoop()
    {
        yield return new WaitForSeconds(intro.length);
        music.clip = loop;
        music.loop = true;
        music.Play();
    }
}
