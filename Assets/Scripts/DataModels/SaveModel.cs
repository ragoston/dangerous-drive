﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SaveModel
{
    public int coins;
    public long[] leaderboards;
    public Unlockable[] cars;
    public Unlockable[] environments;
    public Unlockable[] engines;
    public string selected_car_name;
    public string selected_env_name;
    public string selected_engine_name;
    public bool soundEnabled;

    public SaveModel(int coins, long[] leaderboards, Unlockable[] cars, Unlockable[] environments, Unlockable[] engines, 
        string selected_car_name, string selected_env_name, string selected_engine_name, bool soundEnabled)
    {
        this.coins = coins;
        this.leaderboards = leaderboards;
        this.cars = cars;
        this.environments = environments;
        this.engines = engines;
        this.selected_car_name = selected_car_name;
        this.selected_env_name = selected_env_name;
        this.selected_engine_name = selected_engine_name;
        this.soundEnabled = soundEnabled;
    }
}
