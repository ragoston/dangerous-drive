﻿using UnityEngine;
using System.Collections;

public class PlayerData : MonoBehaviour
{
    public static readonly string SAVE_NAME = "/dangdrive010saves.txt";
    [HideInInspector]
    private SaveModel mSave;
    public SaveModel MSave
    {
        get
        {
            LoadData();
            return mSave;
        }
    }

    public void LoadData()
    {
        if (mSave == null)
        {
            try
            {
                using (System.IO.StreamReader sw = new System.IO.StreamReader(Application.persistentDataPath + SAVE_NAME))
                {
                    //TODO DO ENCODING
                    string json_str = sw.ReadToEnd();
                    mSave = JsonUtility.FromJson<SaveModel>(json_str);
                }
                Debug.Log("Successfully loaded.");
            }
            catch
            {
                //create a new one and save over
                mSave = new SaveModel(0, new long[5] { 0, 0, 0, 0, 0 }, new Unlockable[8] {new Unlockable("car1",true),
                new Unlockable("car2",false),new Unlockable("car3",false),new Unlockable("car4",false),new Unlockable("car5",false)
                ,new Unlockable("car6",false),new Unlockable("car7",false),new Unlockable("car8",false)}, new Unlockable[5] {
                new Unlockable("env1",true),new Unlockable("env2",false),new Unlockable("env3",false),new Unlockable("env4",false),
                new Unlockable("env5",false)}, new Unlockable[3] { new Unlockable("engine1",false), new Unlockable("engine2", false),
                new Unlockable("engine3",false)}, "car1", "env1", null, true);
                SaveData();
                Debug.Log("Didn't find save data, created new one.");
            }
        }
    }

    public void SaveData()
    {
        string json_str = JsonUtility.ToJson(MSave);
        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(Application.persistentDataPath + SAVE_NAME))
        {
            //TODO DO ENCODING
            sw.Write(json_str);
        }
    }

    public int BestUnlockedEngineIndex()
    {
        for (int i = 0; i < MSave.engines.Length; i++)
        {
            if (!MSave.engines[i].isUnlocked)
                return i-1;
        }
        return MSave.engines.Length-1;
    }
}
