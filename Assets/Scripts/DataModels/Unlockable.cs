﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Unlockable
{
    public Unlockable(string name, bool isUnlocked)
    {
        this.name = name;
        this.isUnlocked = isUnlocked;
    }
    public string name;
    public bool isUnlocked;
}
