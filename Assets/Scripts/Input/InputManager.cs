﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public static bool Interaction;
    private Graphic canvas;

    private void Start()
    {
        canvas = GetComponent<Graphic>();
        Interaction = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Interaction = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Interaction = true;
    }
}
