﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System.Linq;
using System;

public class TrackPiece : MonoBehaviour
{
    private static IGameEnder[] mGameEnders;
    private UnityAction<Scene> UnhookReferences;
    private bool didWeDieded;

    private void Start()
    {
        didWeDieded = false;
        UnhookReferences = x => 
        {
            if(mGameEnders != null)
                mGameEnders.ToList().ForEach(y => y = null);
            mGameEnders = null;
        };
        SceneManager.sceneUnloaded += UnhookReferences;
        if (mGameEnders == null)
        {
            mGameEnders = (UnityEngine.Object.FindObjectsOfType(typeof(MonoBehaviour)).Where(x => x is IGameEnder))
                .ToArray().Cast<IGameEnder>().ToArray();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && !didWeDieded)
        {
            didWeDieded = true;
#if UNITY_EDITOR
            Debug.Log(GameObject.FindGameObjectWithTag("Player")
                .transform.position.z);
#endif
            for (int i = 0; i < mGameEnders.Length; i++)
            {
                mGameEnders[i].OnGameEnded(this);
            }
        }
    }
}
