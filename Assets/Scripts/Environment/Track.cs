﻿using UnityEngine;
using System.Collections;
using System;

public class Track : MonoBehaviour, IComparable
{
    public int serial;

    private Transform mEnd;
    public Transform End { get { return mEnd; } }

    public int CompareTo(object obj)
    {
        Track t = obj as Track;
        if (this.serial > t.serial) return 1;
        else if (this.serial < t.serial) return -1;
        return 0;
    }

    private void Start()
    {
        mEnd = transform.Find("end").transform;
    }
}
