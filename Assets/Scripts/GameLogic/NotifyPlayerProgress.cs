﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class NotifyPlayerProgress : MonoBehaviour
{
    private static IOnPlayerProgress pProg;
    public static UnityAction<Scene> UnhookReferences;

    private void Start()
    {
        UnhookReferences = x => { pProg = null; };
        SceneManager.sceneUnloaded += UnhookReferences;
        if(pProg == null)
        {
            try
            {
                pProg = UnityEngine.Object.FindObjectsOfType(typeof(MonoBehaviour)).Where(x => x is IOnPlayerProgress)
                    .FirstOrDefault() as IOnPlayerProgress;
            }
            catch
            {
#if UNITY_EDITOR
                print("Could not find the player progress interface.");
#endif
                //failsafe: the ugly way, using a concrete class :( 
                pProg = UnityEngine.Object.FindObjectOfType<GameLogic>() as IOnPlayerProgress;
            }
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //notify game logic what to do.
            pProg.OnPlayerProgress(this);
        }
    }
}
