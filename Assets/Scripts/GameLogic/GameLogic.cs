﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using Cinemachine;

public class GameLogic : MonoBehaviour, IGameEnder, IGameRestarter
{
    public static readonly float RATIO_WIDE = 70f;
    public static readonly float RATIO_NARROW = 80f;
    private long score = 0;
    private Coroutine mScore;
    public float levelFailWait = 2f;
    private Transform playerTransform;
    private Vector3 playerOriginalPos;
    private GameObject[] cams;
    private GameObject pickupColl;
    private float deltaBack;
    private GameObject[] menus;
    private PlayerData mData;
    private IAdRequester adReq;
    private AudioSource music;
    private AudioSource effects;
    //UI
    private FailedMenu fMenu;
    private InGameUI gUI;
    private CoinCanvas cc;
    private GameObject inputs;

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        playerOriginalPos = playerTransform.position;
        //in case of a narrow screen this way it will be visible at least.
        var a = Screen.width;
        var b = Screen.height;
        if(a < b)
        {
            var temp = a;
            a = b;
            b = temp;
        }
        if(((a*1f) / (b*1f)) > 1.36f)
        {
            GameObject.FindGameObjectWithTag("virtualcam").GetComponent<CinemachineVirtualCamera>()
                .m_Lens.FieldOfView = RATIO_NARROW;
        }

        mData = GetComponent<PlayerData>();
        inputs = GameObject.Find("InputManagement");
        music = GameObject.Find("musicsource").GetComponent<AudioSource>();
        effects = GameObject.Find("effects").GetComponent<AudioSource>();
        inputs.SetActive(true);
        adReq = new BasicAdrequest();
        adReq.MayShowBanner(60);

        menus = GameObject.FindGameObjectsWithTag("NavMenu");
        cc = GameObject.FindGameObjectWithTag("CoinCanvas").GetComponent<CoinCanvas>();
        foreach (var item in menus)
        {
            item.GetComponent<Navigation>().setup();
            item.SetActive(false);
        }
        var mTrack = GameObject.FindGameObjectWithTag("TRACK");
        deltaBack = mTrack.GetComponent<MeshFilter>().mesh.bounds.size.z;
        deltaBack *= mTrack.transform.lossyScale.z;
        
        fMenu = GameObject.FindGameObjectWithTag("FailMenu").GetComponent<FailedMenu>();
        fMenu.setup();
        gUI = GameObject.FindGameObjectWithTag("InGameUI").GetComponent<InGameUI>();

        pickupColl = GameObject.FindGameObjectWithTag("PickupCollection");

        //used to be in restart stuff.

        fMenu.gameObject.SetActive(false);
        mScore = StartCoroutine(IncreaseScore(1f));

        playerTransform.position = playerOriginalPos;
        ResetPickups();
        //anything after the game was restarted.
        playerTransform.FindChildrenByTagRecursive("icon").ForEach(x => x.gameObject.SetActive(false));
        playerTransform.FindChildrenByTagRecursive("icon").Where(x => x.name == mData.MSave.selected_car_name)
            .FirstOrDefault().gameObject.SetActive(true);

        //setting up selected materials
        Material selectedMat = Resources.Load<Material>(mData.MSave.selected_env_name);
        foreach (Transform item in GameObject.FindGameObjectWithTag("wallsCollection").transform)
        {
            item.GetComponent<MeshRenderer>().material = selectedMat;
        }

        Time.timeScale = 1f;
        foreach (var item in menus)
        {
            item.SetActive(false);
        }
        fMenu.gameObject.SetActive(false);
        playerTransform.GetComponent<PlayerControl>().MakeGo();
        gUI.gameObject.SetActive(true);
        score = 0;
        cc.gameObject.SetActive(false);
        //OnRestart();
    }

    public void OnRestart()
    {
        mData.LoadData();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnGameEnded(object sender)
    {
        inputs.SetActive(false);
        gUI.gameObject.SetActive(false);
        StopCoroutine(mScore);
        fMenu.SetCurrentScore(score);
        music.Stop();
        effects.Play();
        if(score > mData.MSave.leaderboards.Min(x => x))
        {
            //then insert into.
            mData.MSave.leaderboards[mData.MSave.leaderboards.MinIndex()] = score;
            mData.SaveData();
        }
        StartCoroutine(WaitThenFailLevel(levelFailWait));
    }

    private int EvaluatePerformance()
    {
        var p_ctrl = playerTransform.GetComponent<PlayerControl>();
        if(score > 20)
        {
            p_ctrl.mCoins += 10;
        }
        else if(score > 60)
        {
            p_ctrl.mCoins += 20;
        }else if(score > 120)
        {
            p_ctrl.mCoins += 40;
        }else if(score > 200)
        {
            p_ctrl.mCoins += 60;
        }else if(score > 500)
        {
            p_ctrl.mCoins += 120;
        }
        Debug.Log(p_ctrl.mCoins + "was the amount of coins you got now.");
        mData.MSave.coins += p_ctrl.mCoins;
        mData.SaveData();
        int _coins = p_ctrl.mCoins;
        p_ctrl.mCoins = 0;
        return _coins;
    }

    private IEnumerator WaitThenFailLevel(float s)
    {
        yield return new WaitForSeconds(s);
        cc.gameObject.SetActive(true);
        cc.ShowAddon(EvaluatePerformance());
        //implement logic
        Time.timeScale = 0;
        //dem ads
        adReq.MayPlayVideo();
        fMenu.gameObject.SetActive(true);      
    }

    private void Update()
    {
        Vector3 pos = playerTransform.position;
        if (Mathf.Abs(pos.z) >= deltaBack)
        {
            pos.z -= deltaBack;
            Vector3 posDelta = pos - playerTransform.position;
            playerTransform.position = pos;

            // Inform any vcams that might be tracking this target
            int numVcams = CinemachineCore.Instance.VirtualCameraCount;
            for (int i = 0; i < numVcams; ++i)
                CinemachineCore.Instance.GetVirtualCamera(i).OnTargetObjectWarped(
                    playerTransform, posDelta);

            ResetPickups();

        }
    }

    private void ResetPickups()
    {
        foreach (Transform c in pickupColl.transform)
        {
            c.gameObject.SetActive(true);
            c.GetComponent<Pickup>().Reset();
        }
    }

    private IEnumerator IncreaseScore(float delay)
    {
        for (; true;)
        {
            yield return new WaitForSeconds(delay);
            score++;
            gUI.SetDistanceProgress(score);
        }
    }
}

internal interface IGameRestarter
{
    void OnRestart();
}