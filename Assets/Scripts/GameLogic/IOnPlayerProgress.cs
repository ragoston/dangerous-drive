﻿public interface IOnPlayerProgress
{
    void OnPlayerProgress(object caller);
}
