﻿internal interface IGameEnder
{
    void OnGameEnded(object sender);
}