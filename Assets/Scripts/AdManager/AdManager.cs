﻿using UnityEngine;
using GoogleMobileAds.Api;
using System;
using UnityEngine.SceneManagement;

public static class AdManager
{
    ////THE REAL THING
    //public static readonly string bannerID = "ca-app-pub-9354630525084491/6039100870";

    ////THE TEST THING
    public static readonly string bannerID = "ca-app-pub-3940256099942544/6300978111";

    ////THE REAL THING
    //public static readonly string videoID = "ca-app-pub-9354630525084491/6745677888";

    ////THE TEST THING
    public static readonly string videoID = "ca-app-pub-3940256099942544/1033173712";

    public static readonly string appID = "ca-app-pub-6623939704209426~3885148585";
    /// <summary>
    /// How many times do we NOT show the vid? After all those time, we actually do.
    /// </summary>
    public static readonly int howManyTimesWithNoVideo = 10;
    private static int toSettingsCounter = 1;
    /// <summary>
    /// If the ToSettingsCounter is 0, we do show a video upon going to "Settings".
    /// </summary>
    public static int ToSettingsCounter
    {
        get
        {
            toSettingsCounter++;
            if (toSettingsCounter >= howManyTimesWithNoVideo)
            {
                toSettingsCounter = 0;

            }
            return toSettingsCounter;
        }
    }
    public static readonly int howManyTimesWithNoVideoInGame = 10;
    private static int inGameVideoAdCounter = 1;
    public static int InGameVideoCounter
    {
        get
        {
            inGameVideoAdCounter++;
            if (inGameVideoAdCounter >= howManyTimesWithNoVideoInGame)
            {
                inGameVideoAdCounter = 0;
            }
            return inGameVideoAdCounter;
        }
    }
    
    public static BannerView bannerAd;
    public static InterstitialAd interstitial;
    private static AdRequest adRequestBANNER;
    public static AdRequest AdRequestBANNER
    {
        get
        {
            if (adRequestBANNER == null)
            {
                adRequestBANNER = CreateAdRequest();
            }
            return adRequestBANNER;
        }
        set
        {
            adRequestBANNER = value;
        }
    }
    private static AdRequest adRequestINTERST;
    public static AdRequest AdRequestINTERST
    {
        get
        {
            if (adRequestINTERST == null)
            {
                try
                {
                    adRequestINTERST = CreateAdRequest();
                }
                catch { }

            }
            return adRequestINTERST;
        }
        set
        {
            adRequestINTERST = value;
        }
    }
    public static void GetSomeAdRequests()
    {
        try
        {
            //request both a banner and a vid
            AdRequestBANNER = CreateAdRequest();
            AdRequestINTERST = CreateAdRequest();
        }
        catch { }

    }
    public static void InitializeAds()
    {
        try
        {
            MobileAds.Initialize(appID);
            OnInterstitialAdditional = null;
        }
        catch
        {
            Debug.Log("ADS COULDN'T INITIALIZE");
        }

    }
    /// <summary>
    /// EDIT THIS TO FORCE TEST OR REAL ADS.
    /// </summary>
    /// <returns></returns>
    public static AdRequest CreateAdRequest()
    {
        //FOR THE REAL THING:
        AdRequest request = null;
        try
        {
            request = new AdRequest.Builder().Build();
        }
        catch { }

        return request;
    }
    public static void RequestBanner(AdPosition location)
    {
        try
        {
            // These ad units are configured to always serve test ads.
#if UNITY_EDITOR
            string adUnitId = "unused";
#elif UNITY_ANDROID
    string adUnitId = bannerID;
#else
    string adUnitId = "unexpected_platform";
#endif

            // Clean up banner ad before creating a new one.
            if (bannerAd != null)
            {
                bannerAd.Destroy();
            }

            // Create a 320x50 banner at the top of the screen.
            bannerAd = new BannerView(adUnitId, AdSize.SmartBanner, location);

            // Register for ad events.
            bannerAd.OnAdLoaded += HandleAdLoaded;
            bannerAd.OnAdFailedToLoad += HandleAdFailedToLoad;
            bannerAd.OnAdOpening += HandleAdOpened;
            bannerAd.OnAdClosed += HandleAdClosed;
            bannerAd.OnAdLeavingApplication += HandleAdLeftApplication;

            // Load a banner ad.
            bannerAd.LoadAd(AdRequestBANNER);
            bannerAd.Hide();
        }
        catch
        {

        }

    }
    public static void ShowBannerAd()
    {
        try
        {
            bannerAd.Show();
        }
        catch { }

    }
    public static void HideBannerAd()
    {
        try
        {
            bannerAd.Hide();
        }
        catch { }

    }
    public static void ShowVideo()
    {
        try
        {
            if (interstitial.IsLoaded())
            {
                interstitial.Show();
            }
        }
        catch { }

    }
    private static void HandleAdLeftApplication(object sender, EventArgs e)
    {

    }

    private static void HandleAdClosed(object sender, EventArgs e)
    {

    }

    private static void HandleAdOpened(object sender, EventArgs e)
    {

    }

    private static void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {

    }

    private static void HandleAdLoaded(object sender, EventArgs e)
    {

    }
    //additional shit to pull off after showing an interstitial.
    public static Action OnInterstitialAdditional;

    public static void RequestInterstitial()
    {
        try
        {
            // These ad units are configured to always serve test ads.
#if UNITY_EDITOR
            string adUnitId = "unused";
#elif UNITY_ANDROID
    string adUnitId = videoID;
#else
    string adUnitId = "unexpected_platform";
#endif

            // Clean up interstitial ad before creating a new one.
            if (interstitial != null)
            {
                interstitial.Destroy();
            }

            // Create an interstitial.
            interstitial = new InterstitialAd(adUnitId);

            // Register for ad events.
            interstitial.OnAdLoaded += HandleInterstitialLoaded;
            interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
            interstitial.OnAdOpening += HandleInterstitialOpened;
            interstitial.OnAdClosed += HandleInterstitialClosed;
            interstitial.OnAdLeavingApplication += HandleInterstitialLeftApplication;

            // Load an interstitial ad.
            interstitial.LoadAd(AdRequestINTERST);
        }
        catch
        {

        }

    }

    private static void HandleInterstitialLeftApplication(object sender, EventArgs e)
    {

    }

    private static void HandleInterstitialClosed(object sender, EventArgs e)
    {
        //back for more.
        try
        {
            RequestInterstitial();
            AdditionalIfThisIsGameLevel(OnInterstitialAdditional);
        }
        catch { }

    }
    private static void AdditionalIfThisIsGameLevel(Action action)
    {
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            //meaning game level
            if (action != null)
            {
                action.Invoke();
            }

        }
    }
    private static void HandleInterstitialOpened(object sender, EventArgs e)
    {
        try
        {
            RequestInterstitial();
            AdditionalIfThisIsGameLevel(OnInterstitialAdditional);
        }
        catch { }

    }

    private static void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {

    }

    private static void HandleInterstitialLoaded(object sender, EventArgs e)
    {

    }
    public static void DestroyAllAds()
    {
        try
        {
            if (bannerAd != null)
            {
                bannerAd.Destroy();
            }
            if (interstitial != null)
            {
                interstitial.Destroy();
            }
        }
        catch
        {

        }

    }
}