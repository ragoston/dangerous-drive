﻿using UnityEngine;
using System.Collections;

public interface IAdRequester
{
    /// <summary>
    /// If the ad manager's counter hit the count, play a video. else show banner. 
    /// </summary>
    void MayPlayVideo();
    /// <summary>
    /// Show an interstitial ad.
    /// </summary>
    void ShowVideo();
    /// <summary>
    /// Show a banner ad.
    /// </summary>
    void ShowBanner();

    /// <summary>
    /// stop showing any sort of ads. 
    /// </summary>
    void DestroyAll();
    /// <summary>
    /// chance between 0-100, the percent chance of showing a banner. else don't show anything.
    /// </summary>
    void MayShowBanner(float chance);
}
