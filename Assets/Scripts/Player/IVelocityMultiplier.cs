﻿internal interface IVelocityMultiplier
{
    float Multiplier { get; }
}