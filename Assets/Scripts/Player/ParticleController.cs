﻿using UnityEngine;
using System.Collections;

public class ParticleController : MonoBehaviour
{
    public int particleCount = 30;
    IVelocityMultiplier mVelocity;
    ParticleSystem mParticles;
    ParticleSystem.EmitParams emit;
    private void Start()
    {
        mVelocity = GetComponentInParent(typeof(IVelocityMultiplier)) as IVelocityMultiplier;
        mParticles = GetComponent<ParticleSystem>();
        emit = new ParticleSystem.EmitParams();
    }

    private void Update()
    {
        emit.velocity *= mVelocity.Multiplier;
        mParticles.Emit(emit, particleCount);
    }
}
