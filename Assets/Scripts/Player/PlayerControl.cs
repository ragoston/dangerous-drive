﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Basic interaction logic to control the player character. 
/// </summary>
public class PlayerControl : MonoBehaviour, IVelocityMultiplier, IPickupGartherer
{
    public int mCoins;
    
    private Rigidbody mRb;
    private IPlayerSpeed mSpeed;
    public float rotSpeed = 90f;
    public float straightSpeed = 8f;
    public float turnSpeed = 8f;
    private bool functional = true;

    public float Multiplier
    {
        get
        {
            return mSpeed.Speed;
        }
    }

    private void Awake()
    {
        mRb = GetComponent<Rigidbody>();
        MakeGo();
    }

    private void FixedUpdate()
    {
        //change the speed. 
        //note I plan to use an interface with nested classes a la Kotlin to keep it separated instead of if-else-if-else.

        mRb.velocity = transform.forward * mSpeed.Speed;
        //set the location of the target.
        /*
         * Do this by adding a value to the world forward axis of the player, 
         * then adding a value to the right or left world axis of the player. 
         * Then the player turns to these coordinates over time.
         * Note for appropriate behaviour we can't do this via parenting and we have to update the position of these coordinates
         * so that they are correct. 
         */

        Vector3 t_Target;
        //doesn't matter, just here to ensure the same value is added to get 45 degrees.
        float incr = 10f;

        if (functional && InputManager.Interaction)
        {
            //default right, so if interact then left.
            //world Z is forward, world X is right.
            t_Target = new Vector3(transform.position.x - incr, transform.position.y, transform.position.z + incr);
        }
        else
        {
            t_Target = new Vector3(transform.position.x + incr, transform.position.y, transform.position.z + incr);
        }
        var dest = Quaternion.LookRotation(t_Target - transform.position, Vector3.up);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, dest, rotSpeed * Time.fixedDeltaTime);
    }



    public void MakeStop()
    {
        mSpeed = new ContinousSpeed(0);
        functional = false;
    }

    public void MakeGo()
    {
        var p_f = GetComponent<Player_failLevel>();
        p_f.OnSetup();
        functional = true;
        mSpeed = new ChangingSpeed(this, 0f, straightSpeed, 2f, () => { mSpeed = new ContinousSpeed(straightSpeed); print("continous now boi"); });
        GetComponent<ParticleSystem>().Clear();
        mCoins = 0;
        GetComponent<FuelTank>().ResetTank();
        p_f.OnMakeGo();
    }

    public void GartherPickup(Pickup.PickupType pickupType)
    {
        switch (pickupType)
        {
            case Pickup.PickupType.fuel:
                OnRefuel();
                break;
            case Pickup.PickupType.coin:
                OnCollectCoin();
                break;
            default:
                Debug.Log("boi");
                break;
        }
    }

    private void OnCollectCoin()
    {
        mCoins++;
    }

    private void OnRefuel()
    {
        GetComponent<FuelTank>().ReFuel();
    }

    //here we have the speed component implementations.
    private class ContinousSpeed : IPlayerSpeed
    {
        private float mSpeed = 1f;
        public float Speed { get { return mSpeed; }}
        public ContinousSpeed() { }
        public ContinousSpeed(float speed)
        {
            mSpeed = speed;
        }
    }

    private class DifferentTurnSpeed : IPlayerSpeed
    {
        private float sSpeed;
        private float tSpeed;
        private float _epsilon = 4f;
        private MonoBehaviour outer;
        public float Speed
        {
            get
            {
                float angle = outer.transform.eulerAngles.y;
                angle = (angle > 180) ? angle - 360 : angle;

                if (angle < -45 + _epsilon || angle > 45 - _epsilon)
                {
                    //we are going straight.
                    return sSpeed;
                }
                else
                {
                    return tSpeed;
                }
            }
        }

        public DifferentTurnSpeed(MonoBehaviour outer,
            float straightSpeed, float turnSpeed)
        {
            sSpeed = straightSpeed;
            tSpeed = turnSpeed;
            this.outer = outer;
        }
    }

    private class ChangingSpeed : IPlayerSpeed
    {
        private float mSpeed;
        private IEnumerator cChange;
        private MonoBehaviour mParent;

        private float fSpeed;
        private float tSpeed;
        private float changeSpeed;

        private Action OnTargetSpeedReached;

        public ChangingSpeed(MonoBehaviour mParent, float fromSpeed, float toSpeed,float changeSpeed, Action OnTargetSpeedReached)
        {
            this.mParent = mParent;
            this.changeSpeed = changeSpeed;
            fSpeed = fromSpeed;
            tSpeed = toSpeed;
            this.OnTargetSpeedReached = OnTargetSpeedReached;
        }

        public float Speed
        {
            get
            {
                if(cChange == null)
                {
                    cChange = ChangeSpeed();
                    mParent.StartCoroutine(cChange); 
                }
                return mSpeed;
            }
        }

        private IEnumerator ChangeSpeed()
        {
            //change value over time.
            for (; mSpeed < tSpeed;)
            {
                mSpeed += changeSpeed * Time.deltaTime;
                yield return null; 
            }
            mSpeed = tSpeed;
            //action to pull off afterwards
            if (OnTargetSpeedReached != null)
                OnTargetSpeedReached();
        }

    }
}

internal interface IPlayerSpeed
{
    float Speed { get;}
}


