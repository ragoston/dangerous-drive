﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player_failLevel : MonoBehaviour, IGameEnder
{
    private PlayerControl mPcontrol;
    private List<Transform> icon;

    private bool exploded;
    private GameObject carParticles;

    private void Start()
    {

    }

    public void OnSetup()
    {
        exploded = false;
        carParticles = transform.FindChildByTagRecursive("CarTrail").gameObject;
        mPcontrol = gameObject.GetComponent<PlayerControl>();
        icon = transform.FindChildrenByTagRecursive("icon");
    }

    public void OnGameEnded(object sender)
    {
        /*
         * shit to pull off:
         * 1. particle effect start
         * 2. stop movement
         * others are done by the game logic :) 
         */
        carParticles.SetActive(false);
        mPcontrol.MakeStop();

        if(sender is FuelTank)
        {
            //nothing to implement yet. 
            //if we ran out of fuel, then the car just stops.
        }
        else
        {
            foreach (var item in icon)
                item.gameObject.SetActive(false);
            if (!exploded)
            {
                exploded = true;
                GetComponent<ParticleSystem>().Play();
            }
        }    
    }

    public void OnMakeGo()
    {
        var c_sys = carParticles.GetComponent<ParticleSystem>();
        exploded = false;
        c_sys.Clear();
        c_sys.Play();
    }
}
