﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Linq;

public class FuelTank : MonoBehaviour
{
    private static IGameEnder[] mGameEnders;
    private UnityAction<Scene> UnhookReferences;
    private PlayerData mData;
    private float[] consumptionRates;
    private int mostRecentEngine;
    private float tankFill = 1f;
    private InGameUI mUI;
    private bool didWeDieded;

    [Tooltip("The time in seconds it takes to empty the fuel tank.")]
    public float timeToEmptyTank = 20f;

    [Tooltip("The amount of fuel the car recovers upon refueling.")]
    public float refuelRate = 0.3f;

    private void Start()
    {
        consumptionRates = new float[4] { timeToEmptyTank, timeToEmptyTank + 5f, timeToEmptyTank + 10f, timeToEmptyTank + 20f };
        didWeDieded = false;
        UnhookReferences = x =>
        {
            if (mGameEnders != null)
                mGameEnders.ToList().ForEach(y => y = null);
            mGameEnders = null;
        };
        SceneManager.sceneUnloaded += UnhookReferences;

        mData = GameObject.Find("GameLogic").GetComponent<PlayerData>();

        mostRecentEngine = mData.BestUnlockedEngineIndex() + 1;
        Debug.Log("fuel consumption: " + consumptionRates[mostRecentEngine]);
        if (mGameEnders == null)
        {
            mGameEnders = (UnityEngine.Object.FindObjectsOfType(typeof(MonoBehaviour)).Where(x => x is IGameEnder))
                .ToArray().Cast<IGameEnder>().ToArray();
        }

        mUI = GameObject.FindGameObjectWithTag("InGameUI").GetComponent<InGameUI>();
    }

    private void Update()
    {
        //deltaTime / timetoempty

        tankFill -= (Time.deltaTime / consumptionRates[mostRecentEngine]);
        mUI.SetFillImage(tankFill);
        if (!didWeDieded && tankFill <= 0f)
        {
            didWeDieded = true;
            mGameEnders.ToList().ForEach(x => x.OnGameEnded(this));
        }
    }

    public void ReFuel()
    {
        tankFill += refuelRate;
        if (tankFill > 1f) tankFill = 1f;
    }

    public void ResetTank()
    {
        tankFill = 1f;
    }
}