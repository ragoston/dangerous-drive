﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class Extensions
{
    /// <summary>
    /// Find the first occurrence of a transform with the given name within a gameobject hierarchy.
    /// Returns null if nothing is found.
    /// </summary>
    /// <param name="mTr"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public static Transform FindChildRecursive(this Transform mTr, string name)
    {
        Transform res = null;
        foreach (Transform c in mTr)
        {
            if (c.name == name)
            {
                return c;
            }
            else
            {
                res = c.FindChildRecursive(name);
                if (res != null) return res;
            }               
        }
        return res;
    }
    
    public static Transform FindChildByTagRecursive(this Transform mTr, string tag)
    {
        Transform res = null;
        foreach (Transform c in mTr)
        {
            if (c.tag == tag)
            {
                return c;
            }
            else
            {
                res = c.FindChildRecursive(tag);
                if (res != null) return res;
            }
        }
        return res;
    }

    public static List<Transform> FindChildrenByTagRecursive(this Transform mTr, string tag)
    {
        List<Transform> res = new List<Transform>();
        foreach (Transform c in mTr)
        {
            if(c.tag == tag)
            {
                res.Add(c);
            }
            else
            {
                var _t = c.FindChildrenByTagRecursive(tag);
                if(_t != null)
                {
                    foreach (var item in _t)
                        res.Add(item);
                }
            }
        }
        if (res.Count > 0)
            return res;
        else return null;
    }

    public static int MinIndex<T>(this IEnumerable<T> sequence)
    where T : IComparable<T>
    {
        int minIndex = -1;
        T minValue = default(T);

        int index = 0;
        foreach (T value in sequence)
        {
            if (value.CompareTo(minValue) < 0 || minIndex == -1)
            {
                minIndex = index;
                minValue = value;
            }
            index++;
        }
        return minIndex;
    }
}
