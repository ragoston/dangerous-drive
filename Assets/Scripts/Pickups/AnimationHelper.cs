﻿using UnityEngine;
using System.Collections;

public class AnimationHelper : MonoBehaviour
{

    private void OnBecameVisible()
    {
        GetComponent<Animation>().Play();
    }

    private void OnBecameInvisible()
    {
        GetComponent<Animation>().Stop();
    }
}
