﻿using UnityEngine;
using System.Collections;

public class Coin : Pickup
{
    private bool exploded;
    ParticleSystem mParticles;
    private AudioSource mSource;
    // Use this for initialization
    void Start()
    {
        mSource = GetComponent<AudioSource>();
        exploded = false;
        mParticles = GetComponent<ParticleSystem>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            (other.GetComponent(typeof(IPickupGartherer)) as IPickupGartherer).GartherPickup(PickupType.coin);
            GetComponentInChildren<MeshRenderer>().enabled = false;
            if (!exploded)
            {
                exploded = true;
                mParticles.Play();
                mSource.Play();
                GetComponent<Collider>().enabled = false;
            }
            StartCoroutine(WaitThenDisableGO(3f));
        }
    }

    private IEnumerator WaitThenDisableGO(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }

    public override void Reset()
    {
        GetComponentInChildren<MeshRenderer>().enabled = true;
        GetComponent<Collider>().enabled = true;
        exploded = false;
    }
}
