﻿using UnityEngine;
using System.Collections;

public class Fuel : Pickup
{
    private int x;
    public float levitateRange = 0.5f;
    public float levitateSpeed = 3f;
    private AudioSource mSource;

    void Start()
    {
        mSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            (other.GetComponent(typeof(IPickupGartherer)) as IPickupGartherer).GartherPickup(PickupType.fuel);
            mSource.Play();
            GetComponent<Collider>().enabled = false;
            GetComponent<Renderer>().enabled = false;
            StartCoroutine(WaitThenDeactivateOneself());
        }
    }

    private void Levitate()
    {
        x++;
        if (x == 3600)
            x = 0;
        transform.localPosition = new Vector3(
            transform.localPosition.x, Mathf.Sin(x * 0.0175f * levitateSpeed) * levitateRange, transform.localPosition.z);
    }

    private IEnumerator WaitThenDeactivateOneself()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }

    public override void Reset()
    {
        //nothing to do here.
        GetComponent<Collider>().enabled = true;
        GetComponent<Renderer>().enabled = true;
    }
}
