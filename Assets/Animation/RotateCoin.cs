﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCoin : MonoBehaviour
{
    public float RotationSpeed = 2f;
    private bool canRot = true;

	void Update ()
    {
        if(canRot)
            transform.Rotate(Vector3.forward * (RotationSpeed * Time.deltaTime));
    }

    private void OnBecameVisible()
    {
        canRot = true;
    }

    private void OnBecameInvisible()
    {
        canRot = false;
    }
}
