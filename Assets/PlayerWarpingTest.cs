﻿using UnityEngine;
using Cinemachine;

public class PlayerWarpingTest : MonoBehaviour 
{
    public float ZSpeed = 100;
    public float RotSpeed = 100;
    public float MaxDistance = 200;

    void Update()
    {
        // If the player moved too far, warp it back
        Vector3 pos = transform.position;
        if (Mathf.Abs(pos.z) >= MaxDistance)
        {
            pos.z = -MaxDistance;
            Vector3 posDelta = pos - transform.position;
            transform.position = pos;

            // Inform any vcams that might be tracking this target
            int numVcams = CinemachineCore.Instance.VirtualCameraCount;
            for (int i = 0; i < numVcams; ++i)
                CinemachineCore.Instance.GetVirtualCamera(i).OnTargetObjectWarped(
                    transform, posDelta);
        }
        // Move the player
        pos += new Vector3(0, 0, ZSpeed * Time.deltaTime);
        Quaternion rot = Quaternion.AngleAxis(RotSpeed * Time.deltaTime, Vector3.up) * transform.rotation;
        transform.SetPositionAndRotation(pos, rot);
	}
}
